package com.zuitt.example;

import java.util.Scanner;

public class a1 {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int n = 0;
        while (true) {
            System.out.print("Input an integer whose factorial will be computed: ");
            String input = s.nextLine();

            try {
                n = Integer.parseInt(input);
                break;
            } catch (NumberFormatException e) {
                System.out.println("Only numbers are accepted for this system.");
            }
        }

        int factorial = 1;
        for (int i = 2; i <= n; i++) {
            factorial *= i;
        }
        System.out.println("The factorial of " + n +" is " + factorial);
    }
}
